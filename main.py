import urllib3
import json

http = urllib3.PoolManager()

url = 'https://api.github.com/users/ikhthiandor/starred?page='

# Names of starred repos are stored in this variable
repo_full_names = []

for i in range(1, 25):
    url = 'https://api.github.com/users/ikhthiandor/starred?page={}'.format(i)

    # Got response as a stream of bytes
    response = http.request('GET', url, headers={'User-Agent': 'fetchmystars' })

    # Got the response, now decode
    decoded = response.data.decode('utf-8')

    # Load the decoded string to JSON
    response_as_json = json.loads(decoded)


    # Get full name of each repo
    for repo in response_as_json:
        repo_full_names.append(repo['full_name'])

    # Now write the repo names to a JSON file

with open('my_starred_repos', 'w', encoding='utf-8') as outfile:
    json.dump(repo_full_names, outfile)
